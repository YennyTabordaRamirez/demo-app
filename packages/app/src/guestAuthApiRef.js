import { createApiRef } from '@backstage/core-plugin-api';

export const guestAuthApiRef = createApiRef({
  id: 'guest-auth',
  description: 'API reference for guest authentication',
});